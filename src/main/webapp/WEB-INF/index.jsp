<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Каталог товаров</title>
    <link href="static/css/index.css" rel="stylesheet">
</head>
<body>
<header>
    <ul class="menu">
        <li><a href="index">Арья Старк</a></li>
        <li><a id="logout" href="login">Выход</a></li>
        <li class="cartt"><a href="">Корзина</a></li>
        <span class="badge" id="citem">0</span>
        <li class="clickable"><a href="">Поиск</a></li>
    </ul>
</header>
<h1 id="stick_menu">Категория товаров № 1</h1>

<hr>
<div class="flex-container">
    <c:forEach items="${products.get('dogs')}" var="product">
        <div class="flex-item" tabindex=0>
            <img src="static/resourse/1.png"/>
            <ul>
                <li class="item_name">${product.getName()}</li>
                <li class="item_price">${product.getPrice()}</li>
            </ul>
            <a href="#">X</a>
            <button class="btn">В корзину</button>
        </div>
    </c:forEach>
</div>


<h1 id="stick_menu">Категория товаров № 2</h1>
<hr>
<div class="flex-container">
    <c:forEach items="${products.get('cats')}" var="product">
        <div class="flex-item" tabindex=0>
            <img src="static/resourse/1.png"/>
            <ul>
                <li class="item_name">${product.getName()}</li>
                <li class="item_price">${product.getPrice()}</li>
            </ul>
            <a href="#">X</a>
            <button class="btn">В корзину</button>
        </div>
    </c:forEach>
</div>

<h1 id="stick_menu">Категория товаров № 3</h1>
<hr>
<div class="flex-container">
    <c:forEach items="${products.get('parrots')}" var="product">
        <div class="flex-item" tabindex=0>
            <img src="static/resourse/1.png"/>
            <ul>
                <li class="item_name">${product.getName()}</li>
                <li class="item_price">${product.getPrice()}</li>
            </ul>
            <a href="#">X</a>
            <button class="btn">В корзину</button>
        </div>
    </c:forEach>
</div>
<div class="popup">
    <div class="message">
        <a class="close" href="">×</a>
        <div class="content1">
            <h1>Поиск</h1>
            <hr>
            <input type="search" placeholder="поиск" class="input"/>
            <button>Найти</button>
            <div class="flex-container clickable">
                <div class="flex-item" tabindex=0>
                    <img src="resourse/1.png"/>
                    <ul>
                        <li class="item_name">Котики</li>
                        <li class="item_price">100</li>
                    </ul>
                </div>
                <div class="flex-item" tabindex=0>
                    <img src="resourse/1.png"/>
                    <ul>
                        <li class="item_name">Котики</li>
                        <li class="item_price">100</li>
                    </ul>
                </div>
                <div class="flex-item" tabindex=0>
                    <img src="resourse/1.png"/>
                    <ul>
                        <li class="item_name">Котики</li>
                        <li class="item_price">100</li>
                    </ul>
                </div>
                <div class="flex-item" tabindex=0>
                    <img src="resourse/1.png"/>
                    <ul>
                        <li class="item_name">Котики</li>
                        <li class="item_price">100</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="cart">
    <div class="message">
        <a class="close" href="">×</a>
        <div class="content2">
            <h1>Корзина</h1>
            <hr>
            <button id="btn_clear">Оформить покупку</button>
            <div class="cprice">Сумма товаров</div>
            <div class="dprice">Скидка 10%</div>
            <div class="tprice">Итого</div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="static/js/index.js"></script>
<script src="static/js/cart.js"></script>
</body>
</html>