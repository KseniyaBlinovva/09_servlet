<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Авторизация</title>
	<link href="static/css/login.css" rel="stylesheet">
</head>
<body>
		<div id="container">
			<h2>Авторизация</h2>
		<hr>
			<form id="form" action="login" method="post">
				<label id="error"></label>
				<input class="input" name="login" type="text" value placeholder="Логин"/>
				<br>
				<input class="input" name="password" type="password" value placeholder="Пароль"/>
				<br>
				<input id="submit" type="submit" value="Вход">
				<p><a href="reg">Регистрация</a></p>
			</form>
		</div>
</body>
</html>