let count = 0;
let coint_price = 0;
let total_price = 0;
$(function(){
	let cprice = $('.cprice');
	let tprice = $('.tprice');
	$('.btn').click(function(ev){
		count++;
		let parent = $(ev.target.parentElement);
		let newElement = parent.clone();
		newElement.removeClass('flex-item');
		newElement.addClass('item');
		coint_price += parseInt(parent.find('.item_price').text());
		cprice.text('Сумма товаров: ' + coint_price);
		total_price = coint_price * 0.9;
		tprice.text('Итого: ' + total_price);
		$('#citem').text(count);
		$('.content2').append(newElement);
		newElement.children('button').hide();
	});
	$('#btn_clear').click(function(ev){
		$('.content2').children('.item').remove();
		cprice.text('Сумма товаров: 0');
		tprice.text('Итого: 0');
		count = 0;
		$('#citem').text(count);
	});
	$(document).on('click','.item a',function(ev) {
		let parent = $(ev.target.parentElement);
		count--;
		$('#citem').text(count);
		console.log('test');
		ev.preventDefault();
		$(parent).remove();
		coint_price -= parseInt(parent.find('.item_price').text());
		cprice.text('Сумма товаров: ' + coint_price);
		total_price = coint_price * 0.9;
		tprice.text('Итого: ' + total_price);
	});

});