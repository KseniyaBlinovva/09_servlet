    $(function() {
        $('.clickable').click(function(ev) {
            ev.preventDefault();
           $('.popup').fadeIn(300);
        });

	    $('.cartt').click(function(ev) {
			ev.preventDefault();
            $('.cart').fadeIn(300);
        });

        $('#logout').click(function() {
           logout();
        });
        $('.flex-item').focus(function(ev) {
			let item = $(ev.target);
			item.children('button').show();
			item.width('100%');
      });
        $('.flex-item').blur(function(ev) {
			let item = $(ev.target);
			item.width('');
			item.children('button').hide();
      });
	  	$('button').mousedown(function(ev){
			ev.preventDefault();
			ev.stopPropagation();
		});
	$('a.close').click(function(ev) {
		ev.preventDefault();
		$('.cart').fadeOut(300);
		$('.popup').fadeOut(300);

	});
   });