package com.epam.edulab.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/reg")
public class RegistrationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/reg.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String repeat = req.getParameter("repeat");
        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        if (login != null && password != null
                && repeat != null
                && firstname != null
                && lastname != null) {
            resp.sendRedirect("index");
        } else {
            resp.sendRedirect("reg");
        }

    }
}

