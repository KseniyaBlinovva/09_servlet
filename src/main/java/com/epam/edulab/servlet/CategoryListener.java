package com.epam.edulab.servlet;

import com.epam.edulab.servlet.domain.Product;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import java.util.*;

@WebListener
public class CategoryListener implements ServletContextListener {
    private Random random = new Random();

    @Override
    public void contextInitialized(ServletContextEvent event) {
        List<Product> dogs = new ArrayList<>();
        List<Product> cats = new ArrayList<>();
        List<Product> parrots = new ArrayList<>();
        Map<String, List<Product>> map = new HashMap<>();
        for (int i = 0; i < 5; i++)
            dogs.add(new Product("Собака", random.nextInt(1000)));
        for (int i = 0; i < 5; i++)
            cats.add(new Product("Кот", random.nextInt(1000)));
        for (int i = 0; i < 5; i++)
            parrots.add(new Product("Попугай", random.nextInt(1000)));
        map.put("dogs", dogs);
        map.put("cats", cats);
        map.put("parrots", parrots);
        event.getServletContext().setAttribute("products", map);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}